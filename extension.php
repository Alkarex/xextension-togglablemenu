<?php
class TogglableMenuExtension extends Minz_Extension {
	public function init() {
		$this->registerTranslates();
		$this->registerHook('js_vars', 	array('TogglableMenuExtension', 'registerJSVars'));
		Minz_View::appendScript($this->getFileUrl('togglablemenu.js', 'js'));
		Minz_View::appendStyle($this->getFileUrl('togglablemenu.css', 'css'));
	}

	 public function handleConfigureAction() {
		$this->registerTranslates();

	        if (Minz_Request::isPost()) {
			$minWidth = Minz_Request::param('togglablemenu_min_width', '840');
			if (is_numeric($minWidth)) {
				FreshRSS_Context::$user_conf->TogglableMenu = [
					'menu_display_width_threshold' => intval($minWidth)
				];
				FreshRSS_Context::$user_conf->save();
			}
        	}
	}

	public function registerJSVars($vars) {
		$vars['togglablemenu']['menu_display_width_threshold'] = FreshRSS_Context::$user_conf->TogglableMenu['menu_display_width_threshold'] ?? 840;
		return $vars;
	}
}
